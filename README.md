# Lab5 Homework -- Integration testing

## Specs

```text
Here is InnoCar Specs:
Budet car price per minute = 29
Luxury car price per minute = 74
Fixed price per km = 10
Allowed deviations in % = 14.000000000000002
Inno discount in % = 12
```

## Values

```python
API_KEY = "AKfycbxReuav_j4FMBmT7i6ozd5E9oVjBOC7pKFyp5VlKjTRqmn7hjoeTDuzSayzMzTKKi2W"
URL = f"https://script.google.com/macros/s/{API_KEY}/exec"
EMAIL = "d.chermnykh@innopolis.university"
```

## BVA

| Parameter          | Value Class                                           |
| :----------------- | :---------------------------------------------------- |
| `type`             | `budget`, `luxury`, random string, not specified      |
| `plan`             | `fixed_price`, `minute`, random string, not specified |
| `distance`         | `0`, `<0`, `>0`, not specified                        |
| `planned_distance` | `0`, `<0`, `>0`, not specified                        |
| `time`             | `0`, `<0`, `>0`, not specified                        |
| `planned_time`     | `0`, `<0`, `>0`, not specified                        |
| `inno_discount`    | `yes`, `no`, random string, not specified             |

## How can I be sure that automated tests are correct?

For that I've added logging and checked the outputs manually. For some of the mismatching cases I've checked the formulas manually and found out that the formulas are correct so we can trust our expected results.

## Test Cases

See:
- [test_cases.txt](test_cases.txt)
- [autotest.txt](./autotest.txt)
- [autotest1.txt](./autotest1.txt)

## Found bugs

- Missing "time" field for per-minute plan results in "price: null" instead of an error
- Fixed Price plan for budget car is calculated incorrectly
- No validation missing or negative `distance` parameter
- No validation for missing or negative `planned_distance` parameter
- No validation for missing `time` parameter
- No validation for missing or negative `planned_time` parameter
- Luxury plan price with per-minute plan without inno discount is computed incorrectly (probably the coefficient is incorrect)
- 0 values are not rejected properly
- fixed_price coefficient is not applied for budged cars
- the deviation is not validated correctly and the price is calculated incorrectly
- inno discount is not computed correctly for budget cars (probably the  coefficient is incorrect)
